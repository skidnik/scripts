#!/bin/bash
setparm(){
sed -ie "$1 s/^\/\///g" $2
sed -ie "$1 s/false/true/g" $2
}
fn="/etc/apt/apt.conf.d/50unattended-upgrades"
ln=$(grep -n 'Remove-Unused-Dependencies' $fn | grep -Eo '^[^:]+')
echo $ln
if [ ! -z $ln ]; then setparm $ln $fn
else echo "no string found"
fi
ln=$(grep -n 'Remove-Unused-Kernel-Packages' $fn | grep -Eo '^[^:]+')
echo $ln
if [ ! -z $ln ]; then setparm $ln $fn
else echo "no string found"
fi
