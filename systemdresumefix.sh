#!/bin/bash
esp=/boot/efi
vmlinuz=
initrfs=
options=
test -d ${esp}/loader || mkdir ${esp}/loader
test -d ${esp}/loader/entries || mkdir ${esp}/loader/entries
vmlinuz="$(cat /proc/cmdline | cut -d' ' -f1 | cut -d'=' -f2 | cut -d'/' -f3)"
initrfs="$(cat /proc/cmdline | cut -d' ' -f1 | cut -d'=' -f2 | cut -d'/' -f3 | sed 's/vmlinuz/initramfs/').img"
options="$(cat /proc/cmdline | awk '{ $1=""; print $0 }')"
#cat << FEH
cat > ${esp}/loader/loader.conf << FEH
default dummy
FEH
#cat << FEH
cat > ${esp}/loader/entries/dummy.conf << FEH
title dummy
linux $vmlinuz
initrd $initrfs
options $options
FEH
# This should allow systemd_logind to access those files, not tested,
# maybe you should apply the rule to the efi mount point, e.g. ${esp}.
# question is whether selinux can handle fat23 partitions
which semanage && semanage fcontext -a -t systemd_logind_sessions_t "${esp}/loader(/.*)?"
