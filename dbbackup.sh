#!/bin/bash
# This script expects unix socket auth in mysql
# This script expects database name as the first argument.
die(){
	echo "$@" 1>&2
	echo "CLeaning up"
	rm -vf "${BACKUP_TMP}/${DUMP_BASE_NAME}.sql.gz"
	exit 1
}
[[ -z $1 ]] && die "Please provide a database name as the first argument"

BACKUP_TMP="${HOME}/tmp"
BACKUP_DEST="${HOME}/backups/mysql"
BACKUP_DAILY_RETENTION='3'
BACKUP_WEEKLY_RETENTION='1'
BACKUP_MONTHLY_RETENTION='1'
BACKUP_YEARLY_RETENTION='1'
DB_NAME="$1"
DB_USER="root"

if [[ $(date +%w) == 0 ]] && (( $BACKUP_WEEKLY_RETENTION ))
then
    btype="weekly"
elif [[ $(date +%-d) == 1 ]] && (( $BACKUP_MONTHLY_RETENTION )) && (( $BACKUP_YEARLY_RETENTION ))
then
    if [[ $(date +%-m) == 1 ]] && (( $BACKUP_YEARLY_RETENTION ))
    then
        btype="yearly"
    else
        btype="monthly"
    fi
else
    (( BACKUP_DAILY_RETENTION )) && btype="daily"
fi
if [[ -z $btype ]]
then
    echo "Backup type not set, means today's retention rule is 0, doing nothing"
    exit 0
fi

DUMP_BASE_NAME="${DB_NAME}_$(date +%y%m%d)_${btype}"
[[ -d $BACKUP_TMP ]] || mkdir -p "$BACKUP_TMP"
mysqldump -v -u "$DB_USER" "$1" | gzip > "${BACKUP_TMP}/${DUMP_BASE_NAME}.sql.gz"
[[ -s ${BACKUP_TMP}/${DUMP_BASE_NAME}.sql.gz ]] || die "Backup file empty, apperently dump failed"
[[ -d $BACKUP_DEST ]] || mkdir -p "$BACKUP_DEST"
mv -v "${BACKUP_TMP}/${DUMP_BASE_NAME}.sql.gz" "$BACKUP_DEST"

echo "Gathering existent backup list"
bckplst="$(find "$BACKUP_DEST" -type f -name "${DB_NAME}*")"
printf '%s\n%s\n' 'Existent backups:' "${bckplst}"
rmlist="$(echo "$bckplst" | grep _daily | sort -d | head -n -$BACKUP_DAILY_RETENTION)$(echo "$bckplst" | grep _weekly | sort -d | head -n -$BACKUP_WEEKLY_RETENTION)$(echo "$bckplst" | grep _monthly | sort -d | head -n -$BACKUP_MONTHLY_RETENTION)$(echo "$bckplst" | grep _yearly | sort -d | head -n -$BACKUP_MONTHLY_RETENTION)"
printf '%s\n%s\n' 'List of backups to be removed:' "$rmlist"
while IFS= read -r file
do
   rm -vf "$file"
done <<<"$rmlist"
