#!/bin/bash
usage(){
	echo " This script expects valid directory names as it's arguments. 
	It will attempt to parse wp-config.php or .env file in the directories specified
       	or in the public_html subdirectory and dump the mysql database according to the info found there.
	Multiple directories can be processed at a time.
	 If no arguments given, it will attempt to process the current directory."
}
wpdbdump(){
dbname="$(sed -n "s/^\s*define\s*(\s*[\"']DB_NAME[\"']\s*,\s*[\"']\(.*\)[\"']\s*);/\1/p" wp-config.php)"
uname="$(sed -n "s/^\s*define\s*(\s*[\"']DB_USER[\"']\s*,\s*[\"']\(.*\)[\"']\s*);/\1/p" wp-config.php)"
upass="$(sed -n "s/^\s*define\s*(\s*[\"']DB_PASSWORD[\"']\s*,\s*[\"']\(.*\)[\"']\s*);/\1/p" wp-config.php)"
dbname="${dbname//$'\r'/}"
uname="${uname//$'\r'/}"
upass="${upass//$'\r'/}"
echo "user: $uname DB: $dbname password: $upass"
mysqldump --user="$uname" --password="$upass" $dbname > $dbname$(date +%d%m%y).sql
wc $dbname$(date +%d%m%y).sql
}
lrvldbdump(){
dbn="$(sed -n -e 's/^[^#,/]*DB_DATABASE=//p' .env)"
dbu="$(sed -n -e 's/^[^#,/]*DB_USERNAME=//p' .env)"
dbp="$(sed -n -e 's/^[^#,/]*DB_PASSWORD=//p' .env)"
# strip double/single quotes:
dbp="${dbp%[\',\"]}"
dbp="${dbp#[\',\"]}"
echo "DB: $dbn user: $dbu password: $dbp"
mysqldump --user=$dbu --password=$dbp $dbn > $dbn$(date +%d%m%y).sql
wc $dbn$(date +%d%m%y).sql
}
wd=$PWD
sd=
while [ $1 ];
do
 if [ -d $1 ];
 then
 sd=$sd"$1 "
 else
 echo "$1 is not a valid directory name"
 usage
 fi
shift
done
if [ $sd ];
then
 for d in $sd
 do 
    cd $d
     if [ -f wp-config.php ];
     then
      wpdbdump
     elif [ -f public_html/wp-config.php ]
     then
      cd public_html
      wpdbdump
     elif [ -f .env ]
     then
      lrvldbdump
     elif [ -f public_html/.env ]
     then
      cd public_html
      lrvldbdump
     else
      echo "wp-config.php or .env not found in $d directory"
     fi
    cd $wd
 done
else
 echo "processing current directory"
  if [ -f wp-config.php ];
  then
   wpdbdump
  elif [ -f .env ]
  then
   lrvldbdump
  else
   echo "nothing to process"
  fi
fi
exit 0

