#!/bin/bash
if [ -z $1 ]
then
echo "This script is meant as a reference only and probably never tested.
Expects a .tar.lzo archive name as an argument.
Meant as a pair to awsbackup.sh. expects archive name as an argument an assume you know what you're doing"
exit 1
fi
tarname="$1"
sitepath="/var/www/webroot/"
tarpath="/home/user/tmp/"
s3path="s3://bucket-name/prefixname/"
btype=
if [ ! -d ${tarpath} ]
then
mkdir ${tarpath}
fi
aws s3 cp ${s3path}${tarname} ${tarpath}${tarname} 
tar -C ${sitepath} --lzop -xf "${tarpath}${tarname}"
uname=$(grep DB_USER ${sitepath}wp-config.php | grep '^[[:blank:]]*[^[:blank:]//]' | awk -F "'" '{print $4}')
dbname=$(grep DB_NAME ${sitepath}wp-config.php | grep '^[[:blank:]]*[^[:blank:]//]' | awk -F "'" '{print $4}')
dbpass=$(grep DB_PASSWORD ${sitepath}wp-config.php | grep '^[[:blank:]]*[^[:blank:]//]' | awk -F "'" '{print $4}')
dumpfile="${sitepath}${dbname}$(echo $tarname | cut -d"-" -f 2).sql"
mysql --user="$uname" --password="$dbpass" $dbname < ${dumpfile}
rm -v ${dumpfile}
rm -v ${tarpath}${tarname}.tar.lzo
