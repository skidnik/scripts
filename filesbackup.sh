#!/bin/bash
# This script expects a directory to back up as the first argument.
die(){
	echo "$@" 1>&2
	echo "Trying to clean up"
	[[ -n $BACKUP_NAME ]] && rm -vf "$BACKUP_TMP/${BACKUP_NAME}"
	exit 1
}

[[ -d $1 ]] || die "Please provide an existent directory as the first argument"

BACKUP_TMP="${HOME}/tmp"
BACKUP_DEST="${HOME}/backups/files"
BACKUP_DAILY_RETENTION='3'
BACKUP_WEEKLY_RETENTION='1'
BACKUP_MONTHLY_RETENTION='1'
BACKUP_YEARLY_RETENTION='1'
BACKUP_TARGET="$1"

if [[ $(date +%w) == 0 ]] && (( $BACKUP_WEEKLY_RETENTION ))
then
    btype="weekly"
elif [[ $(date +%-d) == 1 ]] && (( $BACKUP_MONTHLY_RETENTION )) && (( $BACKUP_YEARLY_RETENTION ))
then
    if [[ $(date +%-m) == 1 ]] && (( $BACKUP_YEARLY_RETENTION ))
    then
        btype="yearly"
    else
        btype="monthly"
    fi
else
    (( BACKUP_DAILY_RETENTION )) && btype="daily"
fi
if [[ -z $btype ]]
then
    echo "Backup type not set, means today's retention rule is 0, doing nothing"
    exit 0
fi

BACKUP_BASE_NAME="$(basename "$BACKUP_TARGET")"
BACKUP_NAME="${BACKUP_BASE_NAME}_$(date +%y%m%d)_${btype}.tar.gz"
[[ -d $BACKUP_TMP ]] || mkdir -p "$BACKUP_TMP"
tar --ignore-failed-read -C "$BACKUP_TARGET" -czf "$BACKUP_TMP/${BACKUP_NAME}" .
[[ $? = 2 ]] && die "tar returned critical error, archive apparently invalid."
[[ -s "$BACKUP_TMP/${BACKUP_NAME}" ]] || die "Archive empty, nothing to store." 
[[ -d $BACKUP_DEST ]] || mkdir -p "$BACKUP_DEST"
mv -v "$BACKUP_TMP/${BACKUP_NAME}" "$BACKUP_DEST"

echo "Gathering existent backup list"
bckplst="$(find "$BACKUP_DEST" -type f -name "${BACKUP_BASE_NAME}*")"
printf '%s\n%s\n' 'Existent backups:' "${bckplst}"
rmlist="$(echo "$bckplst" | grep _daily | sort -d | head -n -$BACKUP_DAILY_RETENTION)$(echo "$bckplst" | grep _weekly | sort -d | head -n -$BACKUP_WEEKLY_RETENTION)$(echo "$bckplst" | grep _monthly | sort -d | head -n -$BACKUP_MONTHLY_RETENTION)$(echo "$bckplst" | grep _yearly | sort -d | head -n -$BACKUP_MONTHLY_RETENTION)"
printf '%s\n%s\n' 'List of backups to be removed:' "$rmlist"
while IFS= read -r file
do
   rm -vf "$file"
done <<<"$rmlist"
