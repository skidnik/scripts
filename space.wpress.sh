#!/bin/bash
help(){
echo "$0 [path/to/search] [-d] [num days old]
     -d remove files interactively"
exit 0
}
del=
findparm=
while [ ! -z $1 ]
do
if [ "$1" = "-d" ]
then del=true; shift
elif [ -d "$1" ]
then path="$1"; shift
elif [[ $1 =~ [0-9]+ ]]
then findparm+="-mtime +$1"; shift
elif [[ $1 =~ -h(elp)?$ ]]
then help
else echo "Unrecognized argument: $1"; exit 1
fi
done
sums=
echo "Searching ${path:=.} for .wpress files"
for file in $( find "${path:=.}" -type f -name '*.wpress' ${findparm}); 
do
du -h "${file}"
sums=$(( sums + $(du "${file}" | cut -f1) ))
[ $del ] && rm -i "${file}"
done

[ -z $sums ] && echo -n "No .wpress files found" || echo -n "Total size: $(numfmt --from-unit=1024 --to=iec-i $sums)"
echo
