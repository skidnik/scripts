#!/bin/bash
( [[ -z $1 ]] || [[ $1 =~ ^-?[0-9]+$ ]] ) || exit 0
api_key=''
offset="${1:-}"

curl -s -d offset="${offset}"-d timeaout=10 https://api.telegram.org/bot"${api_key}"/getUpdates | jq '[.result[].message.chat] | unique_by(.id) | .[] | "\(.type), \(.title//.username): \(.id) "'

