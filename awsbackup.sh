#!/bin/bash
# This here is required for aws config to work from cron
export AWS_CONFIG_FILE=/home/user/.aws/config
# aws cli binary full path
awsbinpath='/home/user/.local/bin/aws'
# Name of the archive, cleanup expects 'auto' to be in the name
tarname="name-$(date +%y%m%d)-auto"
# Directories MUST have trailing slashes!
# What to backup, directory expected
sitepath="/path/to/site/"
# Temp dir name for backup file
tarpath="/somewhere/tmp/"
# AWS S3 bucket address and prefix (aws s3 term for subdirectory)
s3path="s3://bucketname/prefix/"
die(){
echo "$@" 1>&2
exit 1
}
test -d ${sitepath} || die "Directory $sitepath does not exist"
# Check for disk space, exit if less than site directory
if [ $(df --output='avail,target' | awk '$2 == "/" {print $1}' ) -lt $(( $(du -s "${sitepath}" | cut -f1 ) + 1048576 )) ]
then
die 'Not enogh disk space'
fi
# Determine prefix for backup type
btype=
if [ ! -d ${tarpath} ]
then
mkdir ${tarpath}
fi
if [[ $(date +%w) == 0 ]]
then
btype="weekly-"
elif [[ $(date +%-d) == 1 ]]
then
    if [[ $(date +%-m) == 1 ]]
    then
    btype="yearly-"
    else
    btype="monthly-"
    fi
else
btype="daily-"
fi
# end determine prefix
# Database backup: 
# Get DB name, user, pass, expects laravel
dbn="$(sed -n -e 's/^[^#,/]*DB_DATABASE=//p' ${sitepath}.env)"
dbu="$(sed -n -e 's/^[^#,/]*DB_USERNAME=//p' ${sitepath}.env)"
dbp="$(sed -n -e 's/^[^#,/]*DB_PASSWORD=//p' ${sitepath}.env)"
# strip double/single quotes:
dbp="${dbp%[\',\"]}"
dbp="${dbp#[\',\"]}"
# Get DB name, user, pass, expects WordPress
#dbu=$(grep DB_USER ${sitepath}wp-config.php | grep '^[[:blank:]]*[^[:blank:]//]' | awk -F "['\"]" '{print $4}')
#dbn=$(grep DB_NAME ${sitepath}wp-config.php | grep '^[[:blank:]]*[^[:blank:]//]' | awk -F "['\"]" '{print $4}')
#dbp=$(grep DB_PASSWORD ${sitepath}wp-config.php | grep '^[[:blank:]]*[^[:blank:]//]' | awk -F "['\"]" '{print $4}')
dumpfile="${sitepath}${dbn}$(date +%y%m%d).sql"
mysqldump --user="$dbu" --password="$dbp" $dbn > ${dumpfile}
test -s "${dumpfile}" || ( echo 'DB dump failed, removing empty file' && rm -v "${dumpfile}" )
# End db backup
tarfile="${tarpath}${tarname}.tar.lzo"
tar -C "${sitepath}" --lzop -cf "${tarfile}" . 
rm -v "${dumpfile}"
test -s "${tarfile}" || ( rm -v "${tarfile}"; die "Archive creation failed" )
# Upload to AWS S3, add prefix on AWS side
${awsbinpath} s3 cp "${tarfile}" "${s3path}${btype}${tarname}.tar.lzo" --quiet && echo "Uploaded $tarname.tar.lzo to $s3path$btype$tarname.tar.lzo"
rm -v ${tarfile}
${awsbinpath} s3 ls "${s3path}${btype}${tarname}.tar.lzo" || die "Backup upload failed"
# Cleaning up unneded backups, -{value} in head determines how much backups of the type to leave
s3list="$(${awsbinpath} s3 ls ${s3path} | grep auto | awk '{print $4}')" 
rmf="$(echo "$s3list" | grep daily | sort -d | head -n -3)$(echo "$s3list" | grep weekly | sort -d | head -n -1)$(echo "$s3list" | grep monthly | sort -d | head -n -1)$(echo "$s3list" | grep yearly | sort -d | head -n -1)"
for file in $rmf
do
echo "Removing backup $s3path$file"
${awsbinpath} s3 rm ${s3path}${file}
done
