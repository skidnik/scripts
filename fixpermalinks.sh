#!/bin/bash
oldurl="${OLDURL-}"
newurl="${NEWURL-}"
wptp="${WPTP-wp_}"
wpdb="${WPDB-}"
wpdbu="${WPDBU-}"
wpdbp="${WPDBP-}"
usage(){
	echo 'Replace permalinks in wordpress sites database, automatically parses
wp-config.php and uses current siteurl from database as the one to replace
 fixpermalinks [options] <http://new.url>
 -t - table prefix
 -d - database name
 -u - database user
 -p - database password
 -o - old url
 -n - new url, can be supplied w/o -n'
}
die(){
	echo "$@"; exit 1
}
getwpdbparms(){
	[[ -f wp-config.php ]] || die 'wp-config.php not found. Could not determine database name and credentials'
	wptp="$(sed -e 's/\r$//' wp-config.php | grep '$table_prefix' | sed -E "s/.*('|\")(\w+)('|\");/\2/")"
	wpdb="$(sed -e 's/\r$//' wp-config.php | grep DB_NAME | sed -E "s/\w+\s?\(\s?('|\")\w+('|\"),\s?('|\")(.*)('|\")\s?\);/\4/")"
	wpdbu="$(sed -e 's/\r$//' wp-config.php | grep DB_USER | sed -E "s/\w+\s?\(\s?('|\")\w+('|\"),\s?('|\")(.*)('|\")\s?\);/\4/")"
	wpdbp="$(sed -e 's/\r$//' wp-config.php | grep DB_PASSWORD | sed -E "s/\w+\s?\(\s?('|\")\w+('|\"),\s?('|\")(.*)('|\")\s?\);/\4/")"
}
while (( $# ))
do
	case "$1" in
		( -t ) shift; wptp="${1}"; shift;;
		( -d ) shift; wpdb="${1}"; shift;;
		( -u ) shift; wpdbu="${1}"; shift;;
		( -p ) shift; wpdbp="${1}"; shift;;
		( -o ) shift; oldurl="${1}"; shift;;
		( -n ) shift; newurl="${1}"; shift;;
		( http* ) newurl="${1}"; shift;;
		( -h*|--help ) usage; exit 0;;
		( * ) usage; die 'Invalid option/argument';;
	esac
done
[[ -z $wpdb ]] && getwpdbparms
[[ -z $oldurl ]] && oldurl="$(mysql -s -u "${wpdbu}" -p"${wpdbp}" "${wpdb}" <<<"select option_value from ${wptp}options where option_name = 'siteurl';" 2>/dev/null)"
[[ -z $oldurl ]] && die 'Unable to determine current website url'
echo "wptp:$wptp"
echo "wpdb:$wpdb"
echo "wpdbu:$wpdbu"
echo "wpdbp:$wpdbp"
echo "oldurl:$oldurl"
echo "newurl:$newurl"
[[ -z $newurl ]] && die 'No new website url provided'
echo "mysql -v -u "${wpdbu}" -p"${wpdbp}" "${wpdb}" <<EOQ"
cat <<EOQ
UPDATE ${wptp}options SET option_value = replace(option_value, '${oldurl}', '${newurl}') WHERE option_name = 'home' OR option_name = 'siteurl';
UPDATE ${wptp}posts SET guid = REPLACE (guid, '${oldurl}', '${newurl}');
UPDATE ${wptp}posts SET post_content = REPLACE (post_content, '${oldurl}', '${newurl}');
UPDATE ${wptp}postmeta SET meta_value = REPLACE (meta_value, '${oldurl}','${newurl}');
EOQ
